#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_IME 21
#define MAX_PREZIME 21
#define MAX_ZANIMANJE 31

typedef struct radnik_st {
  char ime[MAX_IME];
  char prezime[MAX_PREZIME];
  float plata;
  char zanimanje[MAX_ZANIMANJE];
} RADNIK;

typedef struct cvor_st {
  RADNIK inf;
  struct cvor_st *levi;
  struct cvor_st *desni;
} BCVOR;

FILE *safe_open(char *, char *);
void ucitavanje(FILE *, BCVOR **);

void init(BCVOR **);
BCVOR *formiraj_cvor(RADNIK);
void dodavanje(BCVOR **, BCVOR *);
void ispis_stabla(FILE *, BCVOR *);
void brisanje_stabla(BCVOR **); 
void izmeni_plate(BCVOR *, float); 
float suma_zarade_po_zanimanju(BCVOR *, char *);
int broj_radnika_sa_zanimanjem(BCVOR *, char *);

int main(int arg_num, char **args) {
    if(arg_num != 3) {
        puts("Pogresan broj argumenata!");
        exit(EXIT_FAILURE);
    }

    FILE *in = safe_open(args[1], "r");
    BCVOR *koren;

    init(&koren);

    ucitavanje(in, &koren);
    fclose(in);

    FILE *out = safe_open(args[2], "w");
    izmeni_plate(koren, -0.1);    // smanji platu za 10%
    ispis_stabla(out, koren);
    fclose(out);
 
    char zanimanje[MAX_ZANIMANJE];   
    char naziv_datoteke[MAX_ZANIMANJE + 4];

    printf("Unesite zanimanje: ");
    scanf("%s", zanimanje);
    __fpurge(stdin);

    strcpy(naziv_datoteke, zanimanje);
    strcat(naziv_datoteke, ".txt");

    out = safe_open(naziv_datoteke, "w");

    fprintf(out, "%.2f\n", suma_zarade_po_zanimanju(koren, zanimanje) / broj_radnika_sa_zanimanjem(koren, zanimanje));

    fclose(out);


    brisanje_stabla(&koren);
}

FILE *safe_open(char *name, char *mode){ 
  FILE *fp = fopen(name, mode);
  
  if (fp == NULL) {
    printf("Doslo je do problema prilikom otvaranje datoteke %s.\n", name);
    exit(EXIT_FAILURE);
  }
  
  return fp;
}

void ucitavanje(FILE *in, BCVOR **pkoren) {
    RADNIK temp;

    while(fscanf(in, "%s %s %f %s", temp.ime,
                                    temp.prezime,
                                    &temp.plata,
                                    temp.zanimanje) != EOF) {
        BCVOR *novi = formiraj_cvor(temp);
        dodavanje(pkoren, novi);
    }
}

void init(BCVOR **pkoren) {
    *pkoren = NULL;
}

BCVOR *formiraj_cvor(RADNIK r) {
    BCVOR *novi = (BCVOR *)malloc(sizeof(BCVOR));

    if(novi == NULL) {
        puts("Ne postoji dovoljno memorije za novi cvor stabla!");
        exit(EXIT_FAILURE);
    }

    novi->inf = r;
    novi->levi = NULL;
    novi->desni = NULL;

    return novi;
}

/*
    Dodavanje u stablo vrsi se po kriterijumu, u ovom slucaju
    to je vrednost plate. Ukoliko je plata manja od plate u
    trenutnom cvoru, kretanje se nastavlja u levom, u suprotnom
    slucaju u desnom podstablu.
*/
void dodavanje(BCVOR **pkoren, BCVOR *novi) {
    if(*pkoren == NULL) {
        *pkoren = novi;
        return;
    }

    if((*pkoren)->inf.plata > novi->inf.plata) {
        dodavanje(&(*pkoren)->levi, novi);
    } else {
        dodavanje(&(*pkoren)->desni, novi);
    }
}

/*
    Ispis sortiran u rastucem redosledu.
    Za ispis sortiran u opadajucem redosledu,
    potrebno je samo obrnuti redosled rekurzivnih
    poziva funkcije, odnosno da prvo bude pozvano
    nad desnim, potom nad levim podstablom. 

    Ovo direktno zavisi od toga kako je stablo formirano
    (sta ide na levu stranu, sta na desnu i zasto).
*/
void ispis_stabla(FILE *out, BCVOR *koren) {
    if(koren != NULL) {
        ispis_stabla(out, koren->levi);
        fprintf(out, "%s %s %.2f %s\n", koren->inf.ime,
                                        koren->inf.prezime,
                                        koren->inf.plata,
                                        koren->inf.zanimanje);
        ispis_stabla(out, koren->desni);
    }
}

void brisanje_stabla(BCVOR **pkoren) {
    if(*pkoren != NULL) {
        brisanje_stabla(&(*pkoren)->levi);
        brisanje_stabla(&(*pkoren)->desni);

        free(*pkoren);
        *pkoren = NULL;
    }
}

void izmeni_plate(BCVOR *koren, float procenat) {
    if(koren != NULL) {
        izmeni_plate(koren->levi, procenat);
        izmeni_plate(koren->desni, procenat);

        koren->inf.plata += (procenat * koren->inf.plata);
    }

}

/*
    Princip funkcionisanja naredne dve funkcije je sledeci:
    Sumiraj dobijene vrednosti iz levog i desnog podstabla
    i dodaj vrednost iz trenutnog cvora ukoliko je njegov
    radnik trazenog zanimanja.
*/
float suma_zarade_po_zanimanju(BCVOR *koren, char *zanimanje) {
    if(koren == NULL) {
        return 0;
    }

    float zarada = suma_zarade_po_zanimanju(koren->levi, zanimanje); 
    zarada += suma_zarade_po_zanimanju(koren->desni, zanimanje); 

    return strcmp(koren->inf.zanimanje, zanimanje) ? zarada : (zarada += koren->inf.plata);
    
}

int broj_radnika_sa_zanimanjem(BCVOR *koren, char *zanimanje) {
    if(koren == NULL) {
        return 0;
    }

    int broj = broj_radnika_sa_zanimanjem(koren->levi, zanimanje); 
    broj += broj_radnika_sa_zanimanjem(koren->desni, zanimanje); 

    return strcmp(koren->inf.zanimanje, zanimanje) ? broj : ++broj;   // koriscen infiksni oblik operatora ++ zbog prioriteta operacija
}
