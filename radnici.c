#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_IME 21
#define MAX_PREZIME 21
#define MAX_ZANIMANJE 31

typedef struct radnik_st {
	char ime[MAX_IME];
	char prezime[MAX_PREZIME];
	float plata;
	char zanimanje[MAX_ZANIMANJE];
}RADNIK;

typedef struct cvor {
	RADNIK radnik;
	struct cvor *sledeci;
}CVOR;

FILE *safe_open(char *, char *);
void inicijalizacija(CVOR **);
void ucitavanje(FILE *, CVOR **);
void dodavanje_na_kraj(CVOR **, CVOR *);
void sortirano_dodavanje(CVOR **, CVOR *);
void ispis_jednog(FILE *, RADNIK);
void ispis_liste(FILE *, CVOR *);
void brisanje_po_prezimenu(CVOR **, char *);
void brisanje_svih(CVOR **);
void smanji_platu(RADNIK *);
void bubble_sort_prema_plati(CVOR **);
void zameni_mesta(CVOR **, CVOR **, CVOR **);
int broj_elemenata(CVOR *);
float prosek_po_zanimanju(CVOR *, char*);

int main(int argc, char**argv) {
	if (argc != 3) {
		printf("Niste uneli dovoljno argumenata!\n");
		exit(2);
	}
	
	CVOR *glava;
	inicijalizacija(&glava);
	FILE *in = safe_open(argv[1], "r");
	ucitavanje(in, &glava);
	
	FILE *out = safe_open(argv[2], "w");
	ispis_liste(out, glava);
	
	char odabrano_zanimanje[MAX_ZANIMANJE];
	puts("Unesite zanimanje:");
	__fpurge(stdin);
	gets(odabrano_zanimanje);
	char datoteka_zanimanje[MAX_ZANIMANJE+4];
	strcpy(datoteka_zanimanje, odabrano_zanimanje);
	strcat(datoteka_zanimanje, ".txt");
	
	FILE *out_zanimanje = safe_open(datoteka_zanimanje, "w");
	fprintf(out_zanimanje, "%.2f", prosek_po_zanimanju(glava, odabrano_zanimanje));
	
	/*puts("Pre brisanja Ritchie-a:");
	ispis_liste(stdout, glava);
	brisanje_po_prezimenu(&glava, "Ritchie");
	puts("\nNakon brisanja Ritchie-a:");
	ispis_liste(stdout, glava);
	brisanje_svih(&glava);
	puts("\nNakon brisanja cele liste:");
	ispis_liste(stdout, glava);*/
	
	fclose(in);
	fclose(out);
	fclose(out_zanimanje);
	return 0;
}

FILE *safe_open(char *name, char *mode){ 
	FILE *fp = fopen(name, mode);
	
	if (fp == NULL) {
		printf("Doslo je do problema prilikom otvaranje datoteke %s.\n", name);
		exit(EXIT_FAILURE);
	}
	
	return fp;
}

void inicijalizacija(CVOR **pglava) {
	*pglava = NULL;
}

void ucitavanje(FILE *in, CVOR **pglava) {
	RADNIK temp;
	while(fscanf(in, "%s %s %f %s", temp.ime, temp.prezime, &temp.plata, temp.zanimanje)!=EOF) {
		CVOR *novi = (CVOR *) malloc(sizeof(CVOR));
		novi->radnik = temp;
		novi->sledeci = NULL;
		smanji_platu(&novi->radnik);
		//dodavanje_na_kraj(pglava, novi);
		sortirano_dodavanje(pglava, novi);
	}
}

void dodavanje_na_kraj(CVOR **pglava, CVOR *novi) {
	if (*pglava == NULL) {
		*pglava = novi;
	} else {
		CVOR *tmp = *pglava;
		while (tmp->sledeci != NULL) {
			tmp = tmp->sledeci;
		}
		tmp->sledeci = novi;
	}
}

void sortirano_dodavanje(CVOR **pglava, CVOR *novi) {
   if (*pglava == NULL) {
		*pglava = novi;
	} else {
	   CVOR *tmp = *pglava;
	   CVOR *prethodni = *pglava;
	   while (tmp != NULL && tmp->radnik.plata < novi->radnik.plata) {
         prethodni = tmp;
			tmp = tmp->sledeci;
		}
		
		if (tmp == prethodni) {
		   *pglava = novi;
		} else {
		   prethodni->sledeci = novi;
		}
		novi->sledeci = tmp;
	}
}

void ispis_jednog(FILE *out, RADNIK r) {
	fprintf(out, "%s %s %.0f %s\n", r.ime, r.prezime, r.plata, r.zanimanje);
}

void ispis_liste(FILE *out, CVOR *glava){
	while(glava!=NULL) {
		ispis_jednog(out, glava->radnik);
		glava = glava->sledeci;
	}
}

void brisanje_po_prezimenu(CVOR **pglava, char *prezime) {
	CVOR *za_brisanje;
	if (strcmp((*pglava)->radnik.prezime, prezime) == 0) {
		za_brisanje = (*pglava);
		*pglava = (*pglava)->sledeci;
	} else {
		CVOR *tmp = *pglava;
		while(strcmp(tmp->sledeci->radnik.prezime, prezime)!=0) {
			tmp = tmp->sledeci;
		}
		//Kada se zavrsi sa while petljom, u tmp se nalazi adresa cvora koji PRETHODI onom koji brisemo
		za_brisanje = tmp->sledeci;
		tmp->sledeci = za_brisanje->sledeci;
	}
	za_brisanje->sledeci = NULL;
	free(za_brisanje);
}

void brisanje_svih(CVOR **pglava) {
	CVOR *za_brisanje;
	while(*pglava!=NULL) {
		za_brisanje = *pglava;
		*pglava = (*pglava)->sledeci;
		za_brisanje->sledeci = NULL;
		free(za_brisanje);
	}
}

void smanji_platu(RADNIK *pradnik) {
	pradnik->plata -= pradnik->plata*0.1;
}


int broj_elemenata(CVOR *glava) {
	int ukupno = 0;
	while(glava) {
		ukupno++;
		glava=glava->sledeci;
	}
	
	return ukupno;
}

float prosek_po_zanimanju(CVOR *glava, char *zanimanje) {
	int broj = 0;
	int ukupno = 0;
	while (glava) {
		if (strcmp(glava->radnik.zanimanje, zanimanje) == 0) {
			broj++;
			ukupno+=glava->radnik.plata;
		}
		glava=glava->sledeci;
	}
	return (float)ukupno/broj;
}
