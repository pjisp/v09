#include <stdio.h>
#include <stdlib.h>
#define MAX_IME 21
#define MAX_PREZIME 21
#define MAX_ZANIMANJE 31

typedef struct radnik_st {
	char ime[MAX_IME];
	char prezime[MAX_PREZIME];
	float plata;
	char zanimanje[MAX_ZANIMANJE];
}RADNIK;

typedef struct cvor {
	RADNIK radnik;
	struct cvor *sledeci;
}CVOR;

FILE *safe_open(char *, char *);
void inicijalizacija(CVOR **);
void ucitavanje(FILE *, CVOR **);
void dodavanje_na_kraj(CVOR **, CVOR *);
void ispis_jednog(FILE *, RADNIK);
void ispis_liste(FILE *, CVOR *);
void brisanje_po_prezimenu(CVOR **, char *);
void brisanje_svih(CVOR **);

int main(int argc, char**argv) {
	if (argc != 3) {
		printf("Niste uneli dovoljno argumenata!\n");
		exit(2);
	}
	
	CVOR *glava;
	inicijalizacija(&glava);
	
	FILE *in = safe_open(argv[1], "r");
	ucitavanje(in, &glava);
	fclose(in);

	FILE *out = safe_open(argv[2], "w");
	ispis_liste(out, glava);
	fclose(out);

	puts("Pre brisanja Medine:");
    ispis_liste(stdout, glava);     // ispis na ekran
	brisanje_po_prezimenu(&glava, "Medina");
	puts("\nNakon brisanja Medine:");
	ispis_liste(stdout, glava);     // ispis na ekran

	brisanje_svih(&glava);
	
	return 0;
}

FILE *safe_open(char *name, char *mode){ 
	FILE *fp = fopen(name, mode);
	
	if (fp == NULL) {
		printf("Doslo je do problema prilikom otvaranje datoteke %s.\n", name);
		exit(EXIT_FAILURE);
	}
	
	return fp;
}

void inicijalizacija(CVOR **pglava) {
	*pglava = NULL;
}

void ucitavanje(FILE *in, CVOR **pglava) {
	RADNIK temp;
	while(fscanf(in, "%s %s %f %s", temp.ime, temp.prezime, &temp.plata, temp.zanimanje)!=EOF) {
		CVOR *novi = (CVOR *) malloc(sizeof(CVOR));
		novi->radnik = temp;
		novi->sledeci = NULL;
		dodavanje_na_kraj(pglava, novi);
	}
}

void dodavanje_na_kraj(CVOR **pglava, CVOR *novi) {
	if (*pglava == NULL) {
		*pglava = novi;
	} else {
		CVOR *tmp = *pglava;
		while (tmp->sledeci != NULL) {
			tmp = tmp->sledeci;
		}
		tmp->sledeci = novi;
	}
}

void ispis_jednog(FILE *out, RADNIK r) {
	fprintf(out, "%s %s %.0f %s\n", r.ime, r.prezime, r.plata, r.zanimanje);
}

void ispis_liste(FILE *out, CVOR *glava){
	while(glava!=NULL) {
		ispis_jednog(out, glava->radnik);
		glava = glava->sledeci;
	}
}

void brisanje_po_prezimenu(CVOR **pglava, char *prezime) {
	CVOR *za_brisanje;
	if (strcmp((*pglava)->radnik.prezime, prezime) == 0) {
		za_brisanje = (*pglava);
		*pglava = (*pglava)->sledeci;
	} else {
		CVOR *tmp = *pglava;
		while(strcmp(tmp->sledeci->radnik.prezime, prezime)!=0) {
			tmp = tmp->sledeci;
		}
		//Kada se zavrsi sa while petljom, u tmp se nalazi adresa cvora koji PRETHODI onom koji brisemo
		za_brisanje = tmp->sledeci;
		tmp->sledeci = za_brisanje->sledeci;
	}
	
	free(za_brisanje);
}

void brisanje_svih(CVOR **pglava) {
	CVOR *za_brisanje;
	while(*pglava!=NULL) {
		za_brisanje = *pglava;
		*pglava = (*pglava)->sledeci;
		free(za_brisanje);
	}
}








